import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  tareas;
  tarea;
  indice;
  flag=true;
    constructor() {
      this.tareas=[
        {nombre:'tarea 1',descripcion:'Debes hacer la tarea1'},
        {nombre:'tarea 2',descripcion:'Debes hacer la tarea2'},
        {nombre:'tarea 3',descripcion:'Debes hacer la tarea3'},
  
      ];
      this.tarea={};
  
     }
  ngOnInit() {
  }

  save(){
    this.tareas.push(this.tarea);
    console.log(this.tareas);
    this.tarea={};
   }
   edit(i){
     this.tarea=this.tareas[i];
     this.indice=i;
     this.flag=false;
   }
   delete(i){
     if(confirm("Seguro de eliminar")){
      this.tareas.splice(i,1);
     }
     else{
       alert("ok ´_´ ");
     }
   }
   update(){
      this.tareas[this.indice]=this.tarea;
      this.indice=-1;
      this.flag=true;
      this.tarea={};
   }

}
