import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class NosotrosComponent implements OnInit {
  personas;
  persona: any;
  flag=true;
  indice=-1;
  constructor() {
    this.personas = [
      { nombre: 'nombre 1', apellido: 'apellido 1', edad: 50 },
      { nombre: 'nombre 2', apellido: 'apellido 2', edad: 50 },
      { nombre: 'nombre 3', apellido: 'apellido 3', edad: 50 },

    ];
    this.persona = {};

  }

  save() {
    this.personas.push(this.persona);
    this.persona = {};
  }
  edit(i) {
    this.persona=this.personas[i];
    this.flag=false;
    this.indice=i;
  }
  delete(i){
    this.personas.splice(i,1);
  }
  update(){
    this.personas[this.indice]=this.persona;
    this.flag=true;
    this.persona={};
  }
  ngOnInit() {
  }

}
